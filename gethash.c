#include <stdio.h>
#include <string.h>
#include <windows.h>

#define ATOL 16
#define ATPL 18

int main(int argc, char* argv[]){
	FILE *fp;
	int c;
	int count;
	int length;

	char fileName[256];
	char name[256];

	unsigned char hash[ATPL];
	unsigned char hashstr[256];
	unsigned char* pout = &hashstr[0];

	OPENFILENAME opf;
	ZeroMemory(&opf, sizeof(opf));
	opf.hwndOwner = 0;
	opf.lpstrFilter = "Data File\0NVES;*.NVES;VES;*.VES;conf.F880\0\0";
	opf.nFilterIndex = 1;
	opf.lpstrFile = fileName;
	opf.lpstrFile[0] = '\0';
	opf.nMaxFile = sizeof(fileName);
	opf.lpstrFileTitle = name;
	opf.nMaxFileTitle = sizeof(name);
	opf.lpstrInitialDir = NULL;
	opf.lpstrTitle = "请选择数据文件";
	opf.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	opf.lStructSize = sizeof(opf);
	if(!GetOpenFileName(&opf))
	{
		return -1;
	}

	fp = fopen(fileName, "rb");
	if(fp == NULL){
		perror("Open");
		return -1;
	}

	if(strstr(name, "NVES")){
		length =  ATOL;
	}else{
		length = ATPL;
	}

	if(fseek(fp, -length, SEEK_END) == -1){
		perror("Seek");
		return -1;
	}

	fread(hash, 1, length, fp);

	fclose(fp);

	for(count = 0; count < length; count++){
		if((count % 4) == 0){
			if((count % 16) == 0){
				sprintf(pout, "\n");
				pout++;
			}else{
				sprintf(pout, " ");
				pout++;
			}
		}
		sprintf(pout, "%02x", hash[count] & 0xff);
		pout += 2;
	}

	MessageBox(NULL, hashstr, fileName, MB_OK|MB_ICONINFORMATION);

	return 0;
}
